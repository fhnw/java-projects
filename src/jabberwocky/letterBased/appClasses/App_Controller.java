package jabberwocky.letterBased.appClasses;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Optional;

import jabberwocky.letterBased.ServiceLocator;
import jabberwocky.letterBased.ServiceLocator.Mode;
import jabberwocky.letterBased.abstractClasses.Controller;
import jabberwocky.letterBased.commonClasses.Translator;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextInputDialog;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.WindowEvent;

/**
 * Copyright 2015, FHNW, Prof. Dr. Brad Richards. All rights reserved. This code
 * is licensed under the terms of the BSD 3-clause license (see the file
 * license.txt).
 * 
 * @author Brad Richards
 */
public class App_Controller extends Controller<App_Model, App_View> {
	ServiceLocator serviceLocator;
	ChangeListener<Worker.State> trainingChangeListener; // Reusable listener for training
	private TrainerTask trainerTask = null;

	public App_Controller(App_Model model, App_View view) {
		super(model, view);

		// register ourselves to listen for menu items
		view.menuFileTrain.setOnAction((e) -> train());
		view.menuFileTrainDialog.setOnAction((e) -> trainDialog());
		view.menuFileClear.setOnAction((e) -> clear());

		// register ourselves to listen for button clicks
		view.btnGenerate.setOnAction((e) -> buttonClick());

		// control wrapping of generated text
		// view.btnGenerate.widthProperty()

		serviceLocator = ServiceLocator.getServiceLocator();
		serviceLocator.getLogger().info("Application controller initialized");

		trainingChangeListener = new ChangeListener<Worker.State>() {
			@Override
			public void changed(ObservableValue<? extends Worker.State> observable, Worker.State oldValue,
					Worker.State newValue) {
				if (newValue == Worker.State.SUCCEEDED) trainingFinished();
			}
		};
	}

	public void clear() {
		model.clearTrainingData();
		view.updateStatus();
	}

	public void train() {
		if (trainerTask == null) {
			FileChooser fileChooser = new FileChooser();
			File f = fileChooser.showOpenDialog(view.getStage());
			if (f != null) {
				trainOnFile(f, true);
			}
		}
	}

	public void trainingFinished() {
		// Remove event handlers and bindings
		view.progress.setVisible(false);
		view.progress.progressProperty().unbind();
		trainerTask.stateProperty().removeListener(trainingChangeListener);
		trainerTask = null;

		view.updateStatus();
	}

	/**
	 * The user first selects a directory, in which we will recursively search for
	 * source files. The user then selects a file-ending (for example ".java"), and
	 * we will only match files with that ending. We then train on each file found.
	 */
	public void trainDialog() {
		DirectoryChooser dirChooser = new DirectoryChooser();
		File dir = dirChooser.showDialog(view.getStage());
		if (dir != null) {
			Translator t = serviceLocator.getTranslator();

			TextInputDialog textInputDialog = new TextInputDialog(t.getString("fileExtension.prompt"));
			textInputDialog.setTitle(t.getString("fileExtension.title"));
			textInputDialog.setHeaderText(t.getString("fileExtension.instructions"));
			Optional<String> extension = textInputDialog.showAndWait(); // show() allows the program to continue
																		// immediately

			// Extract from Optional, ensure result starts with .
			if (extension.isPresent()) {
				String ext = extension.get();
				if (ext.startsWith(".")) {
					// Recurse through this directory and all subdirectories
					trainOnDirectory(dir, ext);
				}
			}
		}
	}

	private void trainOnDirectory(File directory, String extension) {
		// Get all directory contents
		File[] contents = directory.listFiles();
		for (File f : contents) {
			if (f.isDirectory()) {
				serviceLocator.getLogger().info("Training on directory " + f.getAbsolutePath());
				trainOnDirectory(f, extension);
			} else if (f.getName().endsWith(extension)) {
				trainOnFile(f, false);
			}
		}
		view.updateStatus();
	}

	/**
	 * Train on a file. We only use the change-listener to update the progress bar
	 * if we are training on a single file. This makes no sense if recursing through
	 * directories, since we don't know how many files we will encounter.
	 */
	private void trainOnFile(File f, boolean singleFile) {
		serviceLocator.getLogger().info("Training on file " + f.getAbsolutePath());
		try (BufferedReader in = new BufferedReader(new FileReader(f))) {
			// Read entire file into a buffer
			StringBuffer sb = new StringBuffer();
			String line = in.readLine();
			while (line != null) {
				sb.append(line);
				sb.append("\n");
				line = in.readLine();
			}
			serviceLocator.setSequenceLength((int) view.sliderNumLetters.getValue());
			serviceLocator.setMode(getModeFromView());

			// If training on a single file, we use a Task attached to a ProgressBar
			// If training on many files, we currently do this sequentially, to avoid
			// synchronization problems. This can and should be optimized to use multiple
			// threads!

			// Set up the training task
			trainerTask = new TrainerTask(model, sb);
			Thread ttt = new Thread(trainerTask);

			if (singleFile) {
				// Bind progress-bar to model's progress property
				view.progress.progressProperty().bind(trainerTask.progressProperty());
				view.progress.setVisible(true);

				// Set up binding for when training is finished
				trainerTask.stateProperty().addListener(trainingChangeListener);
			}

			ttt.start();

			if (!singleFile) {
				ttt.join();
			}

		} catch (Exception e) {
			serviceLocator.getLogger().severe(e.toString());
		}
	}

	private Mode getModeFromView() {
		if (view.rdoChar.isSelected()) return Mode.CharacterMode;
		if (view.rdoWord.isSelected()) return Mode.WordMode1;
		if (view.rdoWord2.isSelected()) return Mode.WordMode2;
		return null; // Should never happen
	}

	public void buttonClick() {
		String out = model.generateText();
		view.txtGeneratedText.setText(out);
	}
}
