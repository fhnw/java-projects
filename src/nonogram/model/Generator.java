package nonogram.model;

import java.util.Random;

import nonogram.model.Model.CellOption;

public class Generator {
	private static Random rand;

	/**
	 * Generate a nonogram of the specified size, starting with the given random
	 * seed. Puzzles must pass checks that they are not trivial, for example, that
	 * there are no rows which can be completed instantly.
	 * 
	 * @param gridSize
	 * @param randomSeed
	 * @return the generated puzzle
	 */
	public static CellOption[][] generateNonogram(int gridSize, long randomSeed) {
		rand = new Random(randomSeed);
		CellOption[][] grid = null;
		boolean success = false;
		while (!success) {
			grid = new CellOption[gridSize][gridSize];
			generate(grid);
			success = checkPuzzle(grid);
		}
		return grid;
	}

	/**
	 * This generation method simply tends to keep following cells the same value as
	 * previous cells, with the given probability. To add more randomness, and to
	 * increase the density, we seed the initial puzzle with a few random blocks,
	 * which are not overwritten in the subsequent generation process.
	 */
	private static void generate(CellOption[][] grid) {
		int gridSize = grid.length;
		// A few random blocks
		for (int i = 0; i < gridSize - 5; i++) {
			int x = (int) (rand.nextFloat() * (gridSize - 5));
			int y = (int) (rand.nextFloat() * (gridSize - 5));
			grid[x][y] = CellOption.BLOCK;
		}

		// Puzzle generation, respecting the blocks just seeded
		for (int i = 0; i < gridSize; i++) {
			CellOption co = grid[i][i] == CellOption.EMPTY
					&& (i == 0 || (grid[i][i - 1] == CellOption.EMPTY && grid[i - 1][i] == CellOption.EMPTY))
							? CellOption.EMPTY
							: CellOption.BLOCK;
			grid[i][i] = nextCellValue(co);
			for (int j = i + 1; j < gridSize; j++) {
				grid[j][i] = nextCellValue(grid[j - 1][i]);
				grid[i][j] = nextCellValue(grid[i][j - 1]);
			}
		}
	}

	/**
	 * Each cell value is based off of the value of the preceding cell
	 * 
	 * @param prev
	 * @return
	 */
	private static CellOption nextCellValue(CellOption prev) {
		final float PROB = 0.9f;
		if (rand.nextFloat() < PROB)
			return prev;
		else
			return (prev == CellOption.EMPTY) ? CellOption.BLOCK : CellOption.EMPTY;
	}

	/**
	 * Check the puzzle for quality. At the moment, the only check is for columns or
	 * rows that are instantly solvable. If any such columns or rows are present,
	 * the puzzle fails the quality check.
	 * 
	 * A row or column is instantly solvable if both end spaces are blocks, and if
	 * there are never two empty spaces next to each other. A row or column is also
	 * instantly solvable if it is entirely empty.
	 * 
	 * @param grid
	 *            - The puzzle
	 * @return true if the puzzle is acceptable, false if not acceptable
	 */
	private static boolean checkPuzzle(CellOption[][] grid) {
		boolean isOk = true;
		int gridSize = grid.length;

		// Check that every row and column has at least one block
		for (int i = 0; i < gridSize && isOk; i++) {
			boolean rowOk = false;
			boolean colOk = false;
			for (int j = 0; j < gridSize && (!rowOk || !colOk); j++) {
				rowOk |= grid[i][j] == CellOption.BLOCK;
				colOk |= grid[j][i] == CellOption.BLOCK;
			}
			isOk &= isOk && rowOk && colOk;
		}
		
		// Check that every row and column has enough empty spaces to have multiple solutions
		for (int i = 0; i < gridSize && isOk; i++) {
			boolean rowOk = grid[i][0] == CellOption.EMPTY || grid[i][gridSize - 1] == CellOption.EMPTY;
			boolean colOk = grid[0][i] == CellOption.EMPTY || grid[gridSize - 1][i] == CellOption.EMPTY;
			for (int j = 0; j < gridSize - 1 && (!rowOk || !colOk); j++) {
				rowOk |= grid[i][j] == CellOption.EMPTY && grid[i][j + 1] == CellOption.EMPTY;
				colOk |= grid[j][i] == CellOption.EMPTY && grid[j + 1][i] == CellOption.EMPTY;
			}
			isOk &= isOk && rowOk && colOk;
		}
		return isOk;
	}
}
