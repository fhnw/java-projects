package nonogram.view;

import javafx.beans.Observable;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextInputDialog;

public class PuzzleKeyDialog extends TextInputDialog {
	private final Button btnOk;

	public PuzzleKeyDialog() {
		super();
			
		this.setTitle("Enter puzzle key");
		this.setHeaderText("Enter puzzle key");
		this.setContentText("Puzzle key:");
		
		this.getEditor().textProperty().addListener(this::validatePuzzleKey);
		
		// We won't allow "OK" if value is invalid
		btnOk = (Button) this.getDialogPane().lookupButton(ButtonType.OK);
		btnOk.setDisable(true);
}
	
	private void validatePuzzleKey(Observable o, String oldValue, String newValue) {
		boolean valid = false;
		try {
			int v = Integer.parseInt(newValue);
			if (v >= 100 && v <= 99999999) valid = true;
		} catch (Exception e) {
			// We don't care
		}
		
        if (valid) {
        	getEditor().setStyle("-fx-text-inner-color: black;");
        	btnOk.setDisable(false);
        } else {
        	getEditor().setStyle("-fx-text-inner-color: red;");
        	btnOk.setDisable(true);
        }
	}
}
