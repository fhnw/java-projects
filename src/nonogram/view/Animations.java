package nonogram.view;

import javafx.animation.ParallelTransition;
import javafx.animation.RotateTransition;
import javafx.animation.ScaleTransition;
import javafx.scene.Node;
import javafx.util.Duration;

public class Animations {
	public static ParallelTransition getShrinkAnimation(Node node) {
		ScaleTransition t1 = new ScaleTransition(Duration.millis(2000));
		t1.setToX(0.01);
		t1.setToY(0.01);
		t1.setAutoReverse(false);
		t1.setCycleCount(1);

		RotateTransition t2 = new RotateTransition(Duration.millis(2000));
		t2.setByAngle(540);
		t2.setCycleCount(1);

		ParallelTransition wonAnimation = new ParallelTransition(t1, t2);
		
		wonAnimation.setNode(node);
		return wonAnimation;
	}
	
	public static ParallelTransition getGrowAnimation(Node node) {
		ScaleTransition t1 = new ScaleTransition(Duration.millis(2000));
		t1.setToX(1);
		t1.setToY(1);
		t1.setAutoReverse(false);
		t1.setCycleCount(1);

		RotateTransition t2 = new RotateTransition(Duration.millis(2000));
		t2.setByAngle(540);
		t2.setCycleCount(1);

		ParallelTransition regrowAnimation = new ParallelTransition(t1, t2);
		
		regrowAnimation.setNode(node);
		return regrowAnimation;		
	}
}
