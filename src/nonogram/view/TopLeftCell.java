package nonogram.view;

import javafx.scene.layout.Pane;

public class TopLeftCell extends Pane {
	public TopLeftCell() {
		super();
		this.getStyleClass().add("grid-cell");
	}
}
