package nonogram.view;

import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import nonogram.model.Model;

public class View {
	private Stage stage;
	private Model model;

	private Menus menus;
	private StatusBar statusBar;
	private PlayingGrid playingGrid;
	
	public View(Stage stage, Model model) {
		this.stage = stage;
		this.model = model;
		
		// Build view components
		menus = new Menus(model);
		statusBar = new StatusBar(model);
		playingGrid = new PlayingGrid(model, this);

		// Place view components
		BorderPane root = new BorderPane();
		root.setTop(menus);
		root.setBottom(statusBar);
		root.setCenter(playingGrid);

		Scene scene = new Scene(root);
		stage.getIcons().add(new Image(getClass().getResourceAsStream("icon.png")));
		scene.getStylesheets().add(getClass().getResource("nonogram.css").toExternalForm());
		stage.setScene(scene);
		stage.setTitle("Nonograms");
	}

	public void start() {
		this.stage.show();
	}
	
	public Stage getStage() {
		return stage;
	}	

	public Menus getMenus() {
		return menus;
	}
	
	public StatusBar getStatusBar() {
		return statusBar;
	}
	
	public PlayingGrid getPlayingGrid() {
		return playingGrid;
	}
}
