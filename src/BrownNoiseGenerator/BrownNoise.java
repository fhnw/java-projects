package BrownNoiseGenerator;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * The audio output portion of this program comes from a StackOverflow answer:
 * https://stackoverflow.com/questions/26963342/generating-colors-of-noise-in-java
 * 
 * Brown noise simulates "brownian motion", which means that the value output is
 * a random change to the previous value. This is a rough simulation of the
 * motion of a particle, which moves randomly from its current position.
 * 
 * In this program, the amount of movement is driven by a parameter "heat". To
 * keep the values from drifting out-of-bounds, an inverse-linear "cooling"
 * factor pushes values towards zero. Finally, the "limit" parameter represents
 * the maximum distance that the value should deviate from zero.
 *
 * Generally speaking, a higher "heat" increases the high-frequency components.
 * A larger limit mostly tends to decrease volume (since the volume must be
 * scaled to the allowable range). Values should ideally be generated from a
 * Gaussian distribution, but in practice the more efficient linear distribution
 * produces equally good results.
 * 
 * The default parameters give a pleasant noise: heat 0.1, limit 5, non-Gaussian
 */
public class BrownNoise extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		BrownNoiseModel model = new BrownNoiseModel();
		BrownNoiseView view = new BrownNoiseView(stage, model);
		BrownNoiseController controller = new BrownNoiseController(model, view);
		view.start();
	}
}
