package BrownNoiseGenerator;

import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class BrownNoiseView {
	private Stage stage;
	private BrownNoiseModel model;
	
	private Label lblHeat = new Label("Heat");
	Slider sliderHeat = new Slider();
	private Label lblLimit = new Label("Limit");
	Slider sliderLimit = new Slider();
	CheckBox chkGaussian = new CheckBox("Gaussian");
	
	public BrownNoiseView(Stage stage, BrownNoiseModel model) {
		this.stage = stage;
		this.model = model;
		
		GridPane root = new GridPane();
		root.getStyleClass().add("gridpane");
		
		sliderHeat.setMin(0); sliderHeat.setMax(1);
		root.add(lblHeat, 0, 0);
		root.add(sliderHeat, 1, 0);
		sliderLimit.setMin(1); sliderLimit.setMax(5);
		root.add(lblLimit, 0, 1);
		root.add(sliderLimit, 1, 1);
		root.add(chkGaussian, 0, 2);
		
		sliderHeat.setValue(model.heat);
		sliderLimit.setValue(model.limit);
		chkGaussian.setSelected(model.useGaussian);
		
		Scene scene = new Scene(root);
		scene.getStylesheets().add(getClass().getResource("styles.css").toExternalForm());
		stage.setTitle("Brown Noise");
		stage.setScene(scene);		
	}

	public void start() {
		stage.show();
	}

}
