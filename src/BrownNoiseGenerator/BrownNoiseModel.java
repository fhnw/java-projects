package BrownNoiseGenerator;

import java.nio.ByteBuffer;
import java.util.Random;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

public class BrownNoiseModel extends Thread {
	private final int SAMPLE_SIZE = 2;
	private final int PACKET_SIZE = 5000;

	// These parameters should be in a GUI
	volatile double heat = 0.1;
	volatile double limit = 5; // range 1-10
	volatile boolean useGaussian = false;

	public BrownNoiseModel() {
		this.setDaemon(true);
		this.start();
	}

	@Override
	public void run() {
		SourceDataLine line = null;

		try {
			AudioFormat format = new AudioFormat(44100, 16, 1, true, true);
			DataLine.Info info = new DataLine.Info(SourceDataLine.class, format, PACKET_SIZE * 2);

			if (!AudioSystem.isLineSupported(info)) {
				throw new LineUnavailableException();
			}

			line = (SourceDataLine) AudioSystem.getLine(info);
			line.open(format);
			line.start();

			ByteBuffer buffer = ByteBuffer.allocate(PACKET_SIZE);

			Random random = new Random();
			double value = 0;
			while (true) {
				buffer.clear();
				for (int i = 0; i < PACKET_SIZE / SAMPLE_SIZE; i++) {
					// The brownian motion, limited to a max of "bounds" if Gaussian) // TODO
					double nextRand;
					if (useGaussian) {
						nextRand = limit+1;
						while (Math.abs(nextRand) > limit)
							nextRand = random.nextGaussian();
					} else { // simple linear
						nextRand = random.nextDouble();
					}

					double move = nextRand * heat;

					// Pull values back to the center (to prevent audible "pops" if values overflow)
					// Has no effect near zero, and an effect equal to maximum heat as the limit is
					// approached.
					double cool = 0 - heat * (value / limit); // Inverse linear repulsion

					value += move + cool;
					buffer.putShort((short) (value * Short.MAX_VALUE / limit));
				}
				line.write(buffer.array(), 0, buffer.position());
			}
		} catch (LineUnavailableException e) {
			e.printStackTrace();
			System.exit(-1);
		} finally {
			line.drain();
			line.close();
		}
	}
}
