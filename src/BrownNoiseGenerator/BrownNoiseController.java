package BrownNoiseGenerator;

public class BrownNoiseController {
	private BrownNoiseModel model;
	private BrownNoiseView view;

	public BrownNoiseController(BrownNoiseModel model, BrownNoiseView view) {
		this.model = model;
		this.view = view;
		
		view.sliderHeat.valueProperty().addListener( (o, oldValue, newValue) -> model.heat = newValue.doubleValue());
		view.sliderLimit.valueProperty().addListener( (o, oldValue, newValue) -> model.limit = newValue.doubleValue());
		view.chkGaussian.selectedProperty().addListener( (o, oldValue, newValue) -> model.useGaussian = newValue);
	}

}
