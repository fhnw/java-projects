package javaTokenizer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;
import javaTokenizer.tokens.*;
import java.util.ArrayList;

public class MainTest {

	public static void main(String[] args) {
		File f = new File("src/javaTokenizer/Tokenizer.java");
		StringBuffer sb = new StringBuffer();
		try (Scanner in = new Scanner(new FileReader(f))) {
			while (in.hasNextLine()) {
				sb.append(in.nextLine());
				sb.append("\n");
			}
			Tokenizer t = new Tokenizer(sb.toString());
			t.tokenize();
			ArrayList<Token> tokens = t.getTokens();
			for (Token token : tokens) {
				System.out.print(token);
			}
		} catch (FileNotFoundException e) {
			System.out.println("File not found!");
		}
	}

}
