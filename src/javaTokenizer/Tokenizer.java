package javaTokenizer;

import java.util.ArrayList;
import javaTokenizer.tokens.*;

public class Tokenizer {
	private final String sourceCode;
	private int cursor; // current position in the source code
	private final ArrayList<Token> tokens;

	public Tokenizer(String sourceCode) {
		this.sourceCode = sourceCode;
		this.cursor = 0;
		tokens = new ArrayList<>();
	}

	public void tokenize() {
		while (cursor < sourceCode.length()) {
			processToken();
		}
	}
	
	public void processToken() {
		char c = sourceCode.charAt(cursor);
		if (Character.isJavaIdentifierStart(c)) {
			String identifier = parseIdentifier();
			ReservedKeywords res = isReserved(identifier);
			if (res != null)
				tokens.add(new ReservedKeyword(res));
			else if (identifier.equals("true") || identifier.equals("false") || identifier.equals("null"))
				tokens.add(new Literal(identifier));
			else if (identifier.equals("instanceof"))
				tokens.add(new Operator(identifier));
			else
				tokens.add(new Identifier(identifier));
		} else if (isLiteralStart(c)) {
			tokens.add(new Literal(parseLiteral()));
		} else if (Character.isWhitespace(c)) {
			skipWhitespace();
		} else if (isBracket(c)) {
			tokens.add(new Bracket(c));
			cursor++;
		} else if (isCommentStart()) {
			parseComment(); // Comments not presently saved
		} else if (c == ';') {
			tokens.add(new Semicolon());
			cursor++;
		} else if (isEllipsis()) {
			tokens.add(new Ellipsis());
			cursor += 3;
		} else if (c == '.') {
			tokens.add(new Period());
			cursor++;
		} else if (c == ',') {
			tokens.add(new Comma());
			cursor++;
		} else if (c == ':') {
			tokens.add(new Colon());
			cursor++;
		} else if (isOperatorStart(c)) {
			tokens.add(new Operator(parseOperator()));
		} else {
			System.out.println("Unidentified token at position " + cursor + ": " + sourceCode.substring(cursor, cursor+30));
			cursor++;
		}
	}

	/**
	 * If the next character begins an identifier, parse that identifier. At this
	 * point, this includes all reserved keywords, as these are identified at a
	 * later stage.
	 * 
	 * The first character of an identifer must be a letter, an underscore or a
	 * dollar-sign Character.isJavaIdentifierStart(int) Subsequent characters must
	 * be letters, digits, dollar-signs or underscores
	 * Character.isJavaIdentifierPart(int)
	 */
	private String parseIdentifier() {
		char c = sourceCode.charAt(cursor);
		StringBuffer sb = new StringBuffer();
		while (Character.isJavaIdentifierPart(c)) {
			sb.append(c);
			c = sourceCode.charAt(++cursor);
		}
		return sb.toString();
	}

	private ReservedKeywords isReserved(String identifier) {
		for (ReservedKeywords rk : ReservedKeywords.values()) {
			if (rk.toString().toLowerCase().equals(identifier)) {
				return rk;
			}
		}
		return null;
	}

	/**
	 * Not totally robust - we define valid characters, and parse everything we see,
	 * as long as it is a valid character. This allows invalid stuff like
	 * 0xxx123.123.123. However, we assume all the Java we are parsing has already
	 * been validated, so this is an acceptable risk.
	 */
	private String parseLiteral() {
		String result;
		char c = sourceCode.charAt(cursor);
		if (c == '\'') { // Character literal: parse until next '
			result = parseUntil('\'');
		} else if (c == '\"') { // String literal: parse until next "
			result = parseUntil('\"');
		} else { // numeric, parse all digits, plus optional 'x', plus optional decimal point,
					// plus optional type specifier
			StringBuffer sb = new StringBuffer();
			while (isNumericLiteralPart(c)) {
				sb.append(c);
				c = sourceCode.charAt(++cursor);
			}
			result = sb.toString();
		}
		return result;
	}

	private String parseUntil(char until) {
		StringBuffer sb = new StringBuffer();
		char c = sourceCode.charAt(cursor++);
		sb.append(c);
		c = sourceCode.charAt(cursor++);
		boolean escaped = false;
		while (c != until || escaped) {
			sb.append(c);
			escaped = (c == '\\' && !escaped);
			c = sourceCode.charAt(cursor++);
		}
		sb.append(c);
		return sb.toString();
	}

	/**
	 * Recognizes the start of literals, except null, true, false
	 */
	private boolean isLiteralStart(int c) {
		return ((c >= '0' && c <= '9') || c == '\'' || c == '\"');
	}

	/**
	 * Recognizes the start of numeric literals
	 */
	private boolean isNumericLiteralStart(int c) {
		return (c >= '0' && c <= '9');
	}

	/**
	 * Recognizes valid characters within numeric literals. We are overly generous
	 * with letters, but there are a *lot* of them, so...
	 */
	private boolean isNumericLiteralPart(int c) {
		return ((c >= '0' && c <= '9') || Character.isLetter(c) || c == '_' || c == '.');
	}

	/**
	 * Skip all whitespace, until we come to something else
	 */
	private void skipWhitespace() {
		while (cursor < sourceCode.length() && Character.isWhitespace(sourceCode.charAt(cursor)))
			cursor++;
	}

	/**
	 * Identify all brackets except <>, which we allow to be identified as
	 * operators. Great, context-sensitive syntax.
	 */
	private boolean isBracket(char c) {
		return (c == '(' || c == ')' || c == '[' || c == ']' || c == '{' || c == '}');
	}

	/**
	 * Identify the ellipsis, which is used for variable parameters
	 */
	private boolean isEllipsis() {
		return (sourceCode.charAt(cursor) == '.' && sourceCode.charAt(cursor + 1) == '.'
				&& sourceCode.charAt(cursor + 2) == '.');
	}
	
	/**
	 * Identify comments, single line and multi-line. At the present time, we simply skip them. Later, we might
	 * add a method "parseComment" to retain them as a kind of token.
	 */
	private boolean isCommentStart() {
		if (sourceCode.charAt(cursor) == '/' && sourceCode.charAt(cursor + 1) == '/') {
			return true;
		} else if (sourceCode.charAt(cursor) == '/' && sourceCode.charAt(cursor + 1) == '*'
				&& sourceCode.charAt(cursor + 2) == '*') {
			return true;
		} else {
			return false;
		}
	}
	
	private void parseComment() {
		if (sourceCode.charAt(cursor) == '/' && sourceCode.charAt(cursor + 1) == '/') {
			while (sourceCode.charAt(cursor) != '\n') cursor++;
		} else if (sourceCode.charAt(cursor) == '/' && sourceCode.charAt(cursor + 1) == '*'
				&& sourceCode.charAt(cursor + 2) == '*') {
			int end = sourceCode.indexOf("*/", cursor);
			cursor = end+2;
		}
	}
	
	/**
	 * Identify an operator (arithmethic, boolean, lambda, etc.)
	 */
	private boolean isOperatorStart(char c) {
		return (c == '<' || c == '>' || c == '+' || c == '=' || c == '*' || c == '/' || c == '%' || c == '-' || c == '&' || c == '|' || c == '!');
	}
	
	private String parseOperator() {
		char c1 = sourceCode.charAt(cursor);
		char c2 = sourceCode.charAt(cursor+1);
		
		if (c2 == '=') { // Valid in all cases!
			cursor += 2;
			return Character.toString(c1) + Character.toString(c2);
		} else if (c1 == '&' && c2 == '&') {
			cursor += 2;
			return "&&";
		} else if (c1 == '|' && c2 == '|') {
			cursor += 2;
			return "||";
		} else if (c1 == '-' && c2 == '>') {
			cursor += 2;
			return "->";
		} else if (c1 == '-' && c2 == '-') {
			cursor += 2;
			return "--";
		} else if (c1 == '+' && c2 == '+') {
			cursor += 2;
			return "++";
		} else {
			cursor++;
			return Character.toString(c1);
		}
	}
	
	public ArrayList<Token> getTokens() {
		return tokens;
	}
}
