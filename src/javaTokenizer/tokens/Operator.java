package javaTokenizer.tokens;

public class Operator extends Token {
	private String strValue;
	
	public Operator(String value) {
		super(TokenType.Operator);
		this.strValue = value;
	}
	
	@Override
	public String toString() {
		return strValue + " "; // Blank only essential for instanceof
	}
}
