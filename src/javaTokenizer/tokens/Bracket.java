package javaTokenizer.tokens;

public class Bracket extends Token {
	private String strValue;

	public Bracket(char c) {
		super(TokenType.Bracket);
		this.strValue = Character.toString(c);
	}
	
	@Override
	public String toString() {
		return strValue;
	}
}
