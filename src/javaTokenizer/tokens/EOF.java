package javaTokenizer.tokens;

public class EOF extends Token {
	public EOF() {
		super(TokenType.EOF);
	}
	
	@Override
	public String toString() {
		return "";
	}
}
