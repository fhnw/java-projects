package javaTokenizer.tokens;

public enum TokenType {
	Reserved, Identifier, Literal, Bracket, Ellipsis, Period,
	Semicolon, Comma, Colon, Operator, Whitespace, EOF
}