package javaTokenizer.tokens;

public class Identifier extends Token {
	private String identifier;
	
	public Identifier(String identifier) {
		super(TokenType.Identifier);
		this.identifier = identifier;
	}
	
	@Override
	public String toString() {
		return identifier + " ";
	}
}
