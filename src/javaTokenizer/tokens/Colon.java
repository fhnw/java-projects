package javaTokenizer.tokens;

public class Colon extends Token {
	public Colon() {
		super(TokenType.Colon);
	}
	
	@Override
	public String toString() {
		return ":";
	}
}
