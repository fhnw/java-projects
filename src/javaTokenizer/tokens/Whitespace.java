package javaTokenizer.tokens;

/**
 * Whitespace isn't saved in the sequence of literals. Hence, the toString
 * method isn't really necessary
 */
public class Whitespace extends Token {
	public Whitespace() {
		super(TokenType.Whitespace);
	}

	@Override
	public String toString() {
		return " ";
	}
}
