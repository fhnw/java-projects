package javaTokenizer.tokens;

/**
 * Identifiers (which include reserved words) are a sequence of letters, digits, underscores, $ (and cannot begin with a digit)
 * @author brad
 *
 */
public abstract class Token {
	TokenType tokenType;
	
	public Token(TokenType tokenType) {
		this.tokenType = tokenType;
	}
}
