package javaTokenizer.tokens;

public class Ellipsis extends Token {
	public Ellipsis() {
		super(TokenType.Ellipsis);
	}
	
	@Override
	public String toString() {
		return "... ";
	}
}
