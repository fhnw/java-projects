package javaTokenizer.tokens;

public class ReservedKeyword extends Token {
	private ReservedKeywords reservedKeyword;
	
	public ReservedKeyword(ReservedKeywords reservedKeyword) {
		super(TokenType.Reserved);
		this.reservedKeyword = reservedKeyword;
	}
	
	@Override
	public String toString() {
		return reservedKeyword.toString().toLowerCase() + " ";
	}
}
