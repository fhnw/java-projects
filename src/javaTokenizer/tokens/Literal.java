package javaTokenizer.tokens;

/**
 * Arguably, this class should actually be a hierarchy of classes. However, we just store
 * all literals as strings, without looking into them further.
 */
public class Literal extends Token {
	public enum LiteralType { NULL, BOOLEAN, CHAR, NUMERIC, STRING };
	private LiteralType literalType;
	private String strValue; // used for String and Numeric literals

	public Literal(String value) {
		super(TokenType.Literal);
		strValue = value;
		if (value.equals("null"))
			literalType = LiteralType.NULL;
		else if (value.equals("true") || value.equals("false")) 
			literalType = LiteralType.BOOLEAN;
		 else if (value.charAt(0) == '\'') 
			literalType = LiteralType.CHAR;
		 else if (Character.isDigit(value.charAt(0))) 
			literalType = LiteralType.NUMERIC;
		else
			literalType = LiteralType.STRING;
	}
	
	@Override
	public String toString() {
		return strValue + " ";
	}
}
