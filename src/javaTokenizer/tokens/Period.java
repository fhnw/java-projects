package javaTokenizer.tokens;

public class Period extends Token {
	public Period() {
		super(TokenType.Period);
	}
	
	@Override
	public String toString() {
		return ".";
	}
}
