package javaTokenizer.tokens;

public class Comma extends Token {
	public Comma() {
		super(TokenType.Comma);
	}
	
	@Override
	public String toString() {
		return ",";
	}
}
