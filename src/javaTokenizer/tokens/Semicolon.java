package javaTokenizer.tokens;

public class Semicolon extends Token {
	public Semicolon() {
		super(TokenType.Semicolon);
	}
	
	@Override
	public String toString() {
		return ";\n";
	}
}
