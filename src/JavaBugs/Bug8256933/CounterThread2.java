package JavaBugs.Bug8256933;

public class CounterThread2 extends Thread {
	
	public static void main(String[] args) {
		Thread[] threads = new Thread[10];
		for (int i = 0; i < 10; ++i) {
			threads[i] = new CounterThread2("Counter-" + i);
		}
		for (Thread t : threads) {
			t.start();
		}
		
		// Wait for all threads to end
		boolean done = false;
		while (!done) {
			boolean alive = false;
			for (int i = 0; i < 10; ++i) {
				if (threads[i].isAlive()) alive = true;
			}
			done = !alive;
		}
		System.out.println("End - end - end");
		System.out.println("Run with " + System.getProperty("java.vendor") + " " + System.getProperty("java.version"));
	}

	// Counter variable
	private static volatile Integer counter = 0;
	
	// Object attributes
	private String threadName;

	public CounterThread2(String name) {
		super(name);
		this.threadName = name;
	}

	public void run() {
		for (int i = 0; i < 100000; i++) {
			synchronized(counter) {
				counter++;
			}
			System.out.println("Count " + counter + " (" + threadName + ")");
		}
	}
}